### Light Shell 45.2 Wallpapers

name: rayleight

license: cc-by-sa

original author: dikasp

#### light

<img height="700" src="/wallpaper/lightshell45-l.png">

#### dark

<img height="700" src="/wallpaper/lightshell45-d.png">

#### alternate

<img height="700" src="/wallpaper/lightshell45-x.png">
