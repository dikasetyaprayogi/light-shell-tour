### Light Shell 45 Tour

Below is some pics of latest Light Shell release. Light Shell also can be used along with limited theming extension.

## Standard GNOME

<img src=/screenshot/ss1.png>
<img src=/screenshot/ss2.png>
<img src=/screenshot/ss3.png>
<img src=/screenshot/ss4.png>

## GNOME mobile / PostmarketOS

<img src=/screenshot/ms.png>

## blurmylightshell + blurmyshell extension

<img src=/screenshot/ss6.png>
<img src=/screenshot/ss7.png>
<img src=/screenshot/ss8.png>

## dash to panel extension

<img src=/screenshot/ss9.png>
<img src=/screenshot/ss10.png>

## Light Shell with custom accent color build

<img src=/screenshot/ss11a.png>
<img src=/screenshot/ss11b.png>


